<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | Palabras de traducción para la personalización del registro/login
    |
    */

    'second_name' => 'Surnames',
    'address' => 'Address',
    'province' => 'Province',

];
