<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreferenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preferencias', function (Blueprint $table) {
            $table->increments('id_preferencia');
            $table->string('preferencia',30);
        });
        
        //inserts ---------------------------------
        DB::table('preferencias')->insert(['preferencia' => 'Deportes']);
        DB::table('preferencias')->insert(['preferencia' => 'Cine']);
        DB::table('preferencias')->insert(['preferencia' => 'Motor']);
        DB::table('preferencias')->insert(['preferencia' => 'Informática']);
        DB::table('preferencias')->insert(['preferencia' => 'Cocina']);
        DB::table('preferencias')->insert(['preferencia' => 'Viajes']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preferencias');
    }
}
