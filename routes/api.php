<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
//use Laravel\Passport\Passport;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('client')->get('/usuarios', 'RegisterUsers@showDataAPI'); //middleware('auth:api')->


