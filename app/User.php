<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    /**
     * @return Array Validator fields
     */
    static function getValidator(){
        return [
            'name' => 'required|max:100',
            'second_name' => 'required|max:100',//['required', 'max:100'],
            'email' => 'required|email',
            'fk_province' => 'required|integer',
            'gender' => 'required|in:male,female',
        ];
    }
    
    /**
     * Inserta los IDs de preferencias asignandolos al ID del usuario
     * @param int $idUsuario Id del usuario recien insertado
     * @param array $preferencias Array de IDs de preferencias
     * 
     * @return boolean <true> si lo ha podido realizar con exito <false> si no ha podido realizarlo con exito
     */
    public function insertPreferencias(int $idUsuario,array $preferencias){
        //datos para trabajar
        $array_insert = [];
        $table_reference = "usuarios_preferencias"; //TODO: habrá que ponerlo en una variable de esta clase
        //creamos el array para la query
        foreach ($preferencias as $key => $value){
            $array_insert[] = [
                'fk_user' => $idUsuario,
                'fk_preferencia' => $value,
            ];
        }
        //retornamos el resultado
        return DB::table($table_reference)->insert($array_insert);
    }
}
